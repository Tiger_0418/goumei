﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class page_hold : MonoBehaviour
{
    public float _RotationSpeed;
    public bool move;
    public GameObject paper;
    public AudioSource right;
    //public Main mn;

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.forward * _RotationSpeed, Space.Self); //物体自转

        this.transform.localPosition = new Vector3(this.transform.localPosition.x, this.transform.localPosition.y, -0.05f);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "frame")
        {
            right.Play();
            //mn.piece_count++;
            paper.SetActive(true);
            this.gameObject.SetActive(false);
        }
    }
}
