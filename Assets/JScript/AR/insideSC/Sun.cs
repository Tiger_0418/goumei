﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class Sun : MonoBehaviour
{
    public GameObject sun_matches;
    public GameObject plane;
    public GameObject lght;
    public int check;
    public GameObject mn;

    // Start is called before the first frame update
    void Start()
    {
        //lght = GameObject.FindGameObjectWithTag("light");
        mn = GameObject.FindGameObjectWithTag("Main");
    }

    // Update is called once per frame
    void Update()
    {
        if (sun_matches.activeSelf)
        {
            sun_matches.transform.localPosition = new Vector3(sun_matches.transform.localPosition.x, sun_matches.transform.localPosition.y, -1.3f);
        }
        if (mn.GetComponent<Main>().sun_firestop == true && check == 0)
        {
            sun_matches.SetActive(false);
            //StartCoroutine(light_fn());
            check = 1;
        }
    }
    IEnumerator light_fn()
    {
        yield return new WaitForSeconds(14f);
        lght.GetComponent<VideoPlayer>().enabled = true;
        yield return new WaitForSeconds(1.5f);
        lght.transform.localPosition = new Vector3(0, 0, 0.2f);
    }
}
