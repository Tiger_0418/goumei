﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class nanshang : MonoBehaviour
{
    public GameObject nanshan_fog_bg;
    public SpriteRenderer nanshan_fog;
    public GameObject nanshan_fog_all;
    public GameObject nanshan_ani;
    public GameObject mn;
    public int check;

    // Start is called before the first frame update
    void Start()
    {
        mn = GameObject.FindGameObjectWithTag("Main");
    }

    // Update is called once per frame
    void Update()
    {
        if (nanshan_fog.color.a <= 0)
        {
            //mn.GetComponent<Main>().ani_bt.transform.localPosition = new Vector3(350, -750);
            //nanshan_fog_all.SetActive(false);
            //check = 1;
        }
        if (mn.GetComponent<Main>().nanshan_boo && check == 0)
        {
            nanshan_fog_all.SetActive(false);
            nanshan_ani.SetActive(true);
            mn.GetComponent<Main>().nanshan_boo = false;
            StartCoroutine(wait_fn());
            check = 1;
        }
    }
    IEnumerator wait_fn()
    {
        yield return new WaitForSeconds(2f);
        nanshan_ani.transform.localPosition = new Vector3(0, 0, 0);
        yield return new WaitForSeconds(2f);
        nanshan_fog_bg.SetActive(false);
    }
}
