﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fire_dis : MonoBehaviour
{
    public Material jagu_paper;
    public float aa = 0;
    public float bb = 0;
    public int cc = 0;

    // Update is called once per frame
    void Update()
    {
        if (aa < 1)
        {
            aa = aa + 0.13f*Time.deltaTime;
        }
        if (aa >= 1)
        {
            aa = 1;
        }
        if (bb < 0.1f && cc == 0)
        {
            bb = bb + 0.03f * Time.deltaTime;
        }
        if (bb >= 0.1f)
        {
            cc = 1;
        }
        if (bb > 0 && cc == 1)
        {
            bb = bb - 0.03f * Time.deltaTime;
        }


        jagu_paper.SetFloat("_Threshold", aa);
        jagu_paper.SetFloat("_EdgeLength", bb);
    }
}
