﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class paper_fade : MonoBehaviour
{
    public Material paper_a;
    public bool go;

    // Start is called before the first frame update
    void Start()
    {
        go = false;
        paper_a.color = new Color(paper_a.color.r, paper_a.color.g, paper_a.color.b, 1);
    }

    // Update is called once per frame
    void Update()
    {
        if (go)
        {
            paper_a.color = new Color(paper_a.color.r, paper_a.color.g, paper_a.color.b, paper_a.color.a - 0.01f);
        }
    }
    public void disapp_fn()
    {
        go = true;
    }
}
