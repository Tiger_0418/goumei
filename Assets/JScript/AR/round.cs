﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class round : MonoBehaviour
{
    public float _RotationSpeed = 0.2f; //定义自转的速度

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.down * _RotationSpeed, Space.Self); //物体自转
    }
}
