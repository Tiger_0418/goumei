﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class firetrigger : MonoBehaviour
{
    public GameObject fire;
    public bool fire_go;
    public bool book_go;
    public Material candle;
    public GameObject candle_fire;

    public Material paper;
    public Material paper_hole;
    public GameObject paper_fired;

    public GameObject book_open;
    public GameObject book_close;
    public GameObject book_close_inside;
    public int once;
    public Main mn;

    // Start is called before the first frame update
    void Start()
    {
        candle.color = new Color(candle.color.r, candle.color.g, candle.color.b, 1);
        paper.color = new Color(paper.color.r, paper.color.g, paper.color.b, 1);
        paper_hole.color = new Color(paper_hole.color.r, paper_hole.color.g, paper_hole.color.b, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (fire_go)
        {
            if (fire.transform.localScale.x <= 0.7f)
            {
                fire.transform.localScale = new Vector3(fire.transform.localScale.x + 0.003f, fire.transform.localScale.y + 0.003f, fire.transform.localScale.z + 0.003f);
            }
            if (candle.color.a >= 0)
            {
                candle.color = new Color(candle.color.r, candle.color.g, candle.color.b, candle.color.a - 0.01f);
                Destroy(candle_fire, 2f);
            }
            
            if (once == 0)
            {
                StartCoroutine(rundown_fn());
                once = 1;
            }
        }
        if (book_go)
        {
            if (paper_fired.transform.localPosition.z >= -1.1f)
            {
                book_open.transform.Translate(Vector3.down * 0.1f * Time.deltaTime, Space.Self);
            }
        }

        if (Input.touchCount == 1)
        {
            //開始觸碰
            if (Input.touches[0].phase == TouchPhase.Began)
            {
                
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "candle")
        {
            fire.SetActive(true);
            //Destroy(fire,4f);
            fire_go = true;
        }
    }
    IEnumerator rundown_fn()
    {
        yield return new WaitForSeconds(10);
        book_close.SetActive(true);
        yield return new WaitForSeconds(2);
        book_close_inside.GetComponent<Animator>().Play("close");
        yield return new WaitForSeconds(1f);
        paper.color = new Color(paper.color.r, paper.color.g, paper.color.b, 0);
        paper_hole.color = new Color(paper_hole.color.r, paper_hole.color.g, paper_hole.color.b, 1);
        yield return new WaitForSeconds(1f);
        mn.ani_bt.SetActive(true);
    }
    public void ani_go()
    {
        StartCoroutine(click_fn());
    }
    IEnumerator click_fn()
    {
        book_close.SetActive(false);
        book_open.SetActive(true);
        paper_fired.SetActive(false);
        yield return new WaitForSeconds(3.5f);
        paper_fired.SetActive(true);
        yield return new WaitForSeconds(2);
        book_go = true;
        yield return new WaitForSeconds(2);
        book_open.transform.localScale = new Vector3(0, 0, 0);
        yield return new WaitForSeconds(1);
        book_go = false;
        paper_fired.GetComponent<Animator>().Play("甲骨文金紙");
    }

}
