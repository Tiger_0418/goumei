﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefebSC : MonoBehaviour
{
    public GameObject target;
    public bool setup;

    // Update is called once per frame
    void Update()
    {
        if (target != null && setup == false)
        {
            this.transform.SetParent(target.transform);
            this.transform.localPosition = new Vector3(0, 0.01f, 0);
            this.transform.localScale = new Vector3(0.04f, 0.04f, 1);
            setup = true;
        }
        else
        {
            target = GameObject.FindGameObjectWithTag("ima_1");
        }
    }
}
