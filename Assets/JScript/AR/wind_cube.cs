﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class wind_cube : MonoBehaviour
{
    public GameObject conmao;
    public bool wait;
    public int check;

    public Material cube_mat_1;
    public Material cube_mat_2;
    public Material cube_mat_3;
    public Material cube_mat_4;
    public Material cube_mat_5;
    public Material cube_mat_6;

    public Material conmao_mat_1;
    public Material conmao_mat_2;
    public Material conmao_mat_3;
    public Material conmao_mat_4;
    public Material conmao_mat_5;
    public Material conmao_mat_6;
    public Material conmao_mat_7;

    // Update is called once per frame
    void Update()
    {
        //if (mat.color.a >= 1 && conmao.activeSelf == false)
        //{
            //wait = false;
        //}
        if (conmao.activeSelf && check == 0)
        {
            StartCoroutine(waitshow_fn());
            check = 1;
        }
        if (wait && cube_mat_1.color.a > 0)
        {
            cube_mat_1.color = new Color(cube_mat_1.color.r, cube_mat_1.color.g, cube_mat_1.color.b, cube_mat_1.color.a - 0.01f);
            cube_mat_2.color = new Color(cube_mat_2.color.r, cube_mat_2.color.g, cube_mat_2.color.b, cube_mat_2.color.a - 0.01f);
            cube_mat_3.color = new Color(cube_mat_3.color.r, cube_mat_3.color.g, cube_mat_3.color.b, cube_mat_3.color.a - 0.01f);
            cube_mat_4.color = new Color(cube_mat_4.color.r, cube_mat_4.color.g, cube_mat_4.color.b, cube_mat_4.color.a - 0.01f);
            cube_mat_5.color = new Color(cube_mat_5.color.r, cube_mat_5.color.g, cube_mat_5.color.b, cube_mat_5.color.a - 0.01f);
            cube_mat_6.color = new Color(cube_mat_6.color.r, cube_mat_6.color.g, cube_mat_6.color.b, cube_mat_6.color.a - 0.01f);

            conmao_mat_1.color = new Color(conmao_mat_1.color.r, conmao_mat_1.color.g, conmao_mat_1.color.b, conmao_mat_1.color.a + 0.01f);
            conmao_mat_2.color = new Color(conmao_mat_2.color.r, conmao_mat_2.color.g, conmao_mat_2.color.b, conmao_mat_2.color.a + 0.01f);
            conmao_mat_3.color = new Color(conmao_mat_3.color.r, conmao_mat_3.color.g, conmao_mat_3.color.b, conmao_mat_3.color.a + 0.01f);
            conmao_mat_4.color = new Color(conmao_mat_4.color.r, conmao_mat_4.color.g, conmao_mat_4.color.b, conmao_mat_4.color.a + 0.01f);
            conmao_mat_5.color = new Color(conmao_mat_5.color.r, conmao_mat_5.color.g, conmao_mat_5.color.b, conmao_mat_5.color.a + 0.01f);
            conmao_mat_6.color = new Color(conmao_mat_6.color.r, conmao_mat_6.color.g, conmao_mat_6.color.b, conmao_mat_6.color.a + 0.01f);
            conmao_mat_7.color = new Color(conmao_mat_7.color.r, conmao_mat_7.color.g, conmao_mat_7.color.b, conmao_mat_7.color.a + 0.01f);
        }
        if (wait == false && check == 1 && cube_mat_1.color.a < 1)
        {
            cube_mat_1.color = new Color(cube_mat_1.color.r, cube_mat_1.color.g, cube_mat_1.color.b, cube_mat_1.color.a + 0.01f);
            cube_mat_2.color = new Color(cube_mat_2.color.r, cube_mat_2.color.g, cube_mat_2.color.b, cube_mat_2.color.a + 0.01f);
            cube_mat_3.color = new Color(cube_mat_3.color.r, cube_mat_3.color.g, cube_mat_3.color.b, cube_mat_3.color.a + 0.01f);
            cube_mat_4.color = new Color(cube_mat_4.color.r, cube_mat_4.color.g, cube_mat_4.color.b, cube_mat_4.color.a + 0.01f);
            cube_mat_5.color = new Color(cube_mat_5.color.r, cube_mat_5.color.g, cube_mat_5.color.b, cube_mat_5.color.a + 0.01f);
            cube_mat_6.color = new Color(cube_mat_6.color.r, cube_mat_6.color.g, cube_mat_6.color.b, cube_mat_6.color.a + 0.01f);

            conmao_mat_1.color = new Color(conmao_mat_1.color.r, conmao_mat_1.color.g, conmao_mat_1.color.b, conmao_mat_1.color.a - 0.01f);
            conmao_mat_2.color = new Color(conmao_mat_2.color.r, conmao_mat_2.color.g, conmao_mat_2.color.b, conmao_mat_2.color.a - 0.01f);
            conmao_mat_3.color = new Color(conmao_mat_3.color.r, conmao_mat_3.color.g, conmao_mat_3.color.b, conmao_mat_3.color.a - 0.01f);
            conmao_mat_4.color = new Color(conmao_mat_4.color.r, conmao_mat_4.color.g, conmao_mat_4.color.b, conmao_mat_4.color.a - 0.01f);
            conmao_mat_5.color = new Color(conmao_mat_5.color.r, conmao_mat_5.color.g, conmao_mat_5.color.b, conmao_mat_5.color.a - 0.01f);
            conmao_mat_6.color = new Color(conmao_mat_6.color.r, conmao_mat_6.color.g, conmao_mat_6.color.b, conmao_mat_6.color.a - 0.01f);
            conmao_mat_7.color = new Color(conmao_mat_7.color.r, conmao_mat_7.color.g, conmao_mat_7.color.b, conmao_mat_7.color.a - 0.01f);
        }
    }
    IEnumerator waitshow_fn()
    {
        wait = true;
        yield return new WaitForSeconds(15);
        wait = false;
        yield return new WaitForSeconds(5.5f);
        check = 0;
        this.gameObject.SetActive(false); ;
    }
}
