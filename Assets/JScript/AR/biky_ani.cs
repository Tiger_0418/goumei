﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class biky_ani : MonoBehaviour
{
    public Animator ani;
    public float time = 0;

    // Update is called once per frame
    void Update()
    {
        time = time - 1f * Time.deltaTime;
        if (time != 0)
        {
            ani.enabled = true;
        }
        if (time <= 0)
        {
            ani.enabled = false;
            time = 0;
        }
    }
    public void ani_go(int xx)
    {
        if (xx == 0)
        {
            time = 1;
        }
    }
}
