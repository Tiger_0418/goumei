﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.Playables;
using UnityEngine.XR;
using UnityEngine.UI;

public class Main : MonoBehaviour
{
    public GameObject sun;
    public GameObject jagu;
    public GameObject piece;
    public GameObject bike;
    public GameObject nanshan;
    public GameObject wind;
    public GameObject shake;
    public AudioSource sun_au;
    public AudioSource jagu_au;
    public AudioSource piece_au;
    public AudioSource bike_au;
    public AudioSource nanshan_au;

    public GameObject jagu_pack;
    public GameObject sun_pack;
    public GameObject nanshan_pack;
    public GameObject bike_pack;
    public GameObject wind_pack;
    public GameObject piece_pack;

    public GameObject piece_timeline;
    public GameObject piece_bg;

    public bool sun_firestop;
    public GameObject sun_plane;
    public int sun_check;

    public GameObject ani_bt;


    //public int piece_count;
    public float piece_countdown = 15;
    public float bike_countdown = 15;
    public float jagu_countdown = 15;
    public float sun_countdown = 15;
    public float nanshan_countdown = 15;
    public float wind_countdown = 15;

    public bool nanshan_boo;
    public int nanshan_check;

    public GameObject bike_game;
    public GameObject bike_ani;

    public GameObject jagu_goldpaper;
    public GameObject jagu_closebook;
    public GameObject jagu_openbook;
    public GameObject jagu_1st;
    public GameObject jagu_2st;
    public bool jagu_touch;
    public int jagu_check;

    public Material jagu_paper;
    public Material book_mat;
    public Material book_mat_L;
    public Material book_mat_R;

    public int jagu_number;
    public int sun_number;
    public int nanshan_number;
    public int bike_number;
    public int wind_number;
    public int piece_number;
    public int shake_number;

    GameObject jagu_pack_cn;
    GameObject sun_pack_cn;
    GameObject nanshan_pack_cn;
    GameObject bike_pack_cn;
    GameObject wind_pack_cn;
    GameObject piece_pack_cn;

    public GameObject wat;
    public GameObject lit;
    public GameObject scan;
    public GameObject audioobj;
    public GameObject arrow;
    public GameObject sound;
    public int sound_check;

    public Material cube_mat_1;
    public Material cube_mat_2;
    public Material cube_mat_3;
    public Material cube_mat_4;
    public Material cube_mat_5;
    public Material cube_mat_6;
    public Material conmao_mat_1;
    public Material conmao_mat_2;
    public Material conmao_mat_3;
    public Material conmao_mat_4;
    public Material conmao_mat_5;
    public Material conmao_mat_6;
    public Material conmao_mat_7;

    // Start is called before the first frame update
    void Start()
    {
        sound_check = PlayerPrefs.GetInt("sound_check");
        if (sound_check == 0)
        {
            sound.SetActive(true);
        }
        StartCoroutine(loaddev_wait("None"));
        /*pos_1 = new Vector3(-6.16f, -0.57f, -0.05f);
        pos_2 = new Vector3(-4.58f, 2.77f, -0.05f);
        pos_3 = new Vector3(-5.41f, -2.07f, -0.05f);
        pos_4 = new Vector3(-2.41f, -3.97f, -0.05f);
        pos_5 = new Vector3(0.78f, -3.64f, -0.05f);
        pos_6 = new Vector3(2.19f, 3.21f, -0.05f);
        pos_7 = new Vector3(3.8f, -2.97f, -0.05f);
        pos_8 = new Vector3(5.01f, 2.41f, -0.05f);
        pos_9 = new Vector3(-1.4f, 3.72f, -0.05f);
        pos_10 = new Vector3(4.39f, 0.07f, -0.05f);*/

        jagu_paper.SetFloat("_Threshold", 0);
        jagu_paper.SetFloat("_EdgeLength", 0);
    }

    // Update is called once per frame
    void Update()
    {
        //甲骨文
        if (jagu_goldpaper == null)
        {
            jagu_goldpaper = GameObject.FindGameObjectWithTag("jagu_goldpaper");
        }
        if (jagu_closebook == null)
        {
            jagu_closebook = GameObject.FindGameObjectWithTag("jagu_closebook");
        }
        if (jagu_openbook == null)
        {
            jagu_openbook = GameObject.FindGameObjectWithTag("jagu_openbook");
        }
        if (jagu_1st == null)
        {
            jagu_1st = GameObject.FindGameObjectWithTag("jagu_1st");
        }
        if (jagu_2st == null)
        {
            jagu_2st = GameObject.FindGameObjectWithTag("jagu_2st");
        }

        if (jagu.activeSelf && jagu_countdown > 0 && jagu_touch == false)
        {
            jagu_countdown = jagu_countdown - 1f * Time.deltaTime;
        }
        if (jagu.activeSelf && jagu_number == 0)
        {
            scan.GetComponent<Animator>().Play("open");
            book_mat.color = new Color(1, 1, 1, 1);
            book_mat_L.color = new Color(1, 1, 1, 1);
            book_mat_R.color = new Color(1, 1, 1, 1);

            jagu_pack_cn = Instantiate(jagu_pack,jagu.transform);
            jagu_au.GetComponent<AudioSource>().Play();
            jagu_number = 1;
        }
        if (jagu.activeSelf == false && jagu_number == 1)
        {
            scan.GetComponent<Animator>().Play("close");
            jagu_au.GetComponent<AudioSource>().Stop();

            jagu_paper.SetFloat("_Threshold", 0);
            jagu_paper.SetFloat("_EdgeLength", 0);
            Destroy(jagu_pack_cn);
            jagu_number = 0;
            jagu_countdown = 15;
            jagu_check = 0;
            jagu_touch = false;
            ani_bt.transform.localPosition = new Vector3(3000, ani_bt.transform.localPosition.y, ani_bt.transform.localPosition.z);
            
        }
        if (jagu_goldpaper != null && jagu_goldpaper.activeSelf == false && jagu_check == 0 && jagu.activeSelf)
        {
            jagu_countdown = 0;
            ani_bt.transform.localPosition = new Vector3(0, ani_bt.transform.localPosition.y, ani_bt.transform.localPosition.z);
            jagu_check = 1;
        }

        //太陽
        if (sun_plane == null)
        {
            sun_plane = GameObject.FindGameObjectWithTag("sun_plane");
        }
        if (sun.activeSelf && sun_countdown > 0)
        {
            sun_countdown = sun_countdown - 1f * Time.deltaTime;
        }
        if (sun.activeSelf && sun_number == 0)
        {
            scan.GetComponent<Animator>().Play("open");

            sun_pack_cn = Instantiate(sun_pack, sun.transform);
            sun_au.GetComponent<AudioSource>().Play();
            sun_number = 1;
        }

        if (sun.activeSelf == false && sun_number == 1)
        {
            scan.GetComponent<Animator>().Play("close");
            sun_au.GetComponent<AudioSource>().Stop();

            Destroy(sun_pack_cn);
            sun_number = 0;
            sun_countdown = 15;
            sun_firestop = false;
            sun_check = 0;
            lit.GetComponent<VideoPlayer>().enabled = false;
            lit.transform.localPosition = new Vector3(0, 0, 10000);
            ani_bt.transform.localPosition = new Vector3(3000, ani_bt.transform.localPosition.y, ani_bt.transform.localPosition.z);
        }
        if (sun_countdown <= 0 && sun_check == 0)
        {
            sun_countdown = 0;
            ani_bt.transform.localPosition = new Vector3(0, ani_bt.transform.localPosition.y, ani_bt.transform.localPosition.z);
            sun_check = 1;
        }

        //詩人的憂鬱
        if (piece_timeline == null)
        {
            piece_timeline = GameObject.FindGameObjectWithTag("piece_timeline");
        }
        if (piece_bg == null)
        {
            piece_bg = GameObject.FindGameObjectWithTag("piece_bg");
        }
        if (piece.activeSelf && piece_number == 0)
        {
            scan.GetComponent<Animator>().Play("open");

            piece_pack_cn = Instantiate(piece_pack, piece.transform);
            piece_au.GetComponent<AudioSource>().Play();
            piece_number = 1;
        }
        if (piece.activeSelf && piece_countdown > 0)
        {
            piece_countdown = piece_countdown - 1f * Time.deltaTime;
        }
        if (piece.activeSelf == false && piece_number == 1)
        {
            scan.GetComponent<Animator>().Play("close");
            piece_au.GetComponent<AudioSource>().Stop();

            Destroy(piece_pack_cn);
            piece_number = 0;
            piece_countdown = 15;
            wat.GetComponent<VideoPlayer>().enabled = false;
            wat.transform.localPosition = new Vector3(0,0,10000);
            ani_bt.transform.localPosition = new Vector3(3000, ani_bt.transform.localPosition.y, ani_bt.transform.localPosition.z);
        }
        if (piece_countdown < 0)
        {
            piece_countdown = 0;
            ani_bt.transform.localPosition = new Vector3(0, ani_bt.transform.localPosition.y, ani_bt.transform.localPosition.z);
        }
        //苦成
        if (bike_ani == null)
        {
            bike_ani = GameObject.FindGameObjectWithTag("bike_ani");
        }
        if (bike_game == null)
        {
            bike_game = GameObject.FindGameObjectWithTag("bike_game");
        }
        if (bike.activeSelf && bike_number == 0)
        {
            scan.GetComponent<Animator>().Play("open");
            arrow.GetComponent<Animator>().SetBool("switch", true);

            bike_pack_cn = Instantiate(bike_pack, bike.transform);
            bike_au.GetComponent<AudioSource>().Play();
            bike_number = 1;
        }
        if (bike.activeSelf && bike_countdown > 0)
        {
            bike_countdown = bike_countdown - 1f * Time.deltaTime;
        }
        if (bike.activeSelf == false && bike_number == 1)
        {
            scan.GetComponent<Animator>().Play("close");
            bike_au.GetComponent<AudioSource>().Stop();
            arrow.GetComponent<Animator>().SetBool("switch", false);

            Destroy(bike_pack_cn);
            bike_number = 0;
            bike_countdown = 15;
            ani_bt.transform.localPosition = new Vector3(3000, ani_bt.transform.localPosition.y, ani_bt.transform.localPosition.z);
        }
        if (bike_countdown < 0)
        {
            bike_countdown = 0;
            ani_bt.transform.localPosition = new Vector3(0, ani_bt.transform.localPosition.y, ani_bt.transform.localPosition.z);
        }
        //南山之夢
        if (nanshan.activeSelf && nanshan_countdown > 0)
        {
            nanshan_countdown = nanshan_countdown - 1f * Time.deltaTime;
        }
        if (nanshan.activeSelf && nanshan_number == 0)
        {
            arrow.GetComponent<Animator>().SetBool("switch",true);

            scan.GetComponent<Animator>().Play("open");
            nanshan_au.GetComponent<AudioSource>().Play();
            nanshan_pack_cn = Instantiate(nanshan_pack, nanshan.transform);
            nanshan_number = 1;
        }
        if (nanshan_countdown <= 0 && nanshan_check == 0)
        {
            nanshan_countdown = 0;
            ani_bt.transform.localPosition = new Vector3(0, ani_bt.transform.localPosition.y, ani_bt.transform.localPosition.z);
            nanshan_check = 1;
        }
        if (nanshan.activeSelf == false && nanshan_number == 1)
        {
            scan.GetComponent<Animator>().Play("close");
            arrow.GetComponent<Animator>().SetBool("switch", false);
            nanshan_au.GetComponent<AudioSource>().Stop();

            Destroy(nanshan_pack_cn);
            nanshan_number = 0;
            nanshan_countdown = 15;
            nanshan_check = 0;
            ani_bt.transform.localPosition = new Vector3(3000, ani_bt.transform.localPosition.y, ani_bt.transform.localPosition.z);
        }
        //風景
        if (wind.activeSelf && wind_number == 0)
        {
            scan.GetComponent<Animator>().Play("open");

            wind_pack_cn = Instantiate(wind_pack, wind.transform);

            cube_mat_1.color = new Color(cube_mat_1.color.r, cube_mat_1.color.g, cube_mat_1.color.b, 1);
            cube_mat_2.color = new Color(cube_mat_2.color.r, cube_mat_2.color.g, cube_mat_2.color.b, 1);
            cube_mat_3.color = new Color(cube_mat_3.color.r, cube_mat_3.color.g, cube_mat_3.color.b, 1);
            cube_mat_4.color = new Color(cube_mat_4.color.r, cube_mat_4.color.g, cube_mat_4.color.b, 1);
            cube_mat_5.color = new Color(cube_mat_5.color.r, cube_mat_5.color.g, cube_mat_5.color.b, 1);
            cube_mat_6.color = new Color(cube_mat_6.color.r, cube_mat_6.color.g, cube_mat_6.color.b, 1);

            conmao_mat_1.color = new Color(conmao_mat_1.color.r, conmao_mat_1.color.g, conmao_mat_1.color.b, 0);
            conmao_mat_2.color = new Color(conmao_mat_2.color.r, conmao_mat_2.color.g, conmao_mat_2.color.b, 0);
            conmao_mat_3.color = new Color(conmao_mat_3.color.r, conmao_mat_3.color.g, conmao_mat_3.color.b, 0);
            conmao_mat_4.color = new Color(conmao_mat_4.color.r, conmao_mat_4.color.g, conmao_mat_4.color.b, 0);
            conmao_mat_5.color = new Color(conmao_mat_5.color.r, conmao_mat_5.color.g, conmao_mat_5.color.b, 0);
            conmao_mat_6.color = new Color(conmao_mat_6.color.r, conmao_mat_6.color.g, conmao_mat_6.color.b, 0);
            conmao_mat_7.color = new Color(conmao_mat_7.color.r, conmao_mat_7.color.g, conmao_mat_7.color.b, 0);

            wind_number = 1;
        }
        if (wind.activeSelf == false && wind_number == 1)
        {
            scan.GetComponent<Animator>().Play("close");

            Destroy(wind_pack_cn);
            wind_number = 0;
        }
        //震顫
        if (shake.activeSelf && shake_number == 0)
        {
            scan.GetComponent<Animator>().Play("open");

            shake_number = 1;
        }
        if (shake.activeSelf == false && shake_number == 1)
        {
            scan.GetComponent<Animator>().Play("close");
            shake_number = 0;
        }
    }
    IEnumerator waitshow_fn(GameObject xx)
    {
        yield return new WaitForSeconds(2f);
        xx.transform.localPosition = new Vector3(0, 0, 0);
    }
    public void ani_fn()
    {
        if (sun.activeSelf)
        {
            sun_firestop = true;
            sun_plane.GetComponent<VideoPlayer>().enabled = true;
            sun_plane.GetComponent<PlayableDirector>().enabled = true;
            StartCoroutine(waitshow_fn(sun_plane));
        }
        if (piece.activeSelf)
        {
            piece_timeline.GetComponent<PlayableDirector>().enabled = true;
            piece_bg.SetActive(false);
        }
        if (bike.activeSelf)
        {
            bike_ani.GetComponent<VideoPlayer>().enabled = true;
            bike_ani.GetComponent<AudioSource>().enabled = true;
            bike_game.GetComponent<AudioSource>().Stop();
            Destroy(bike_game, 2f);
            StartCoroutine(waitshow_fn(bike_ani));
        }
        if (nanshan.activeSelf)
        {
            nanshan_boo = true;
        }
        if (jagu.activeSelf)
        {
            jagu_1st.GetComponent<PlayableDirector>().enabled = false;
            jagu_2st.GetComponent<PlayableDirector>().enabled = true;
            jagu_closebook.SetActive(false);
        }

        ani_bt.transform.localPosition = new Vector3(3000, ani_bt.transform.localPosition.y, ani_bt.transform.localPosition.z);
    }
    public void exit()
    {
        sound_check = 0;
        PlayerPrefs.SetInt("sound_check", sound_check);
        Application.Quit();
    }
    public void home()
    {
        Application.LoadLevel("Home");
    }
    public void sound_remeber()
    {
        sound_check = 1;
        PlayerPrefs.SetInt("sound_check", sound_check);
    }
    IEnumerator loaddev_wait(string newdev)
    {
        XRSettings.LoadDeviceByName(newdev);
        yield return null;
        XRSettings.enabled = false;
        yield return new WaitForSeconds(1);
        audioobj.SetActive(true);
        arrow.SetActive(true);
    }
    
}
