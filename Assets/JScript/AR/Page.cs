﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Page : MonoBehaviour
{
    public GameObject page;
    public GameObject book;
    public Material book_mat;
    public Material book_mat_L;
    public Material book_mat_R;
    public int check;
    public bool fade;

    GameObject page_clone;
    GameObject[] pgs;

    // Start is called before the first frame update
    void Start()
    {
        book_mat.color = new Color(1, 1, 1, 1);
        book_mat_L.color = new Color(1, 1, 1, 1);
        book_mat_R.color = new Color(1, 1, 1, 1);
    }

    // Update is called once per frame
    void Update()
    {
        if (check == 0)
        {
            pgs = GameObject.FindGameObjectsWithTag("pgs");
            for (int x = 0; x < pgs.Length;x++)
            {
                Destroy(pgs[x]);
            }
            StartCoroutine(page_bron());
            check = 1;
        }
        if (fade && book_mat.color.a > 0)
        {
            book_mat.color = new Color(1, 1, 1, book_mat.color.a - 0.01f);
            book_mat_L.color = new Color(1, 1, 1, book_mat_L.color.a - 0.01f);
            book_mat_R.color = new Color(1, 1, 1, book_mat_R.color.a - 0.01f);
        }

    }
    IEnumerator page_bron()
    {
        yield return new WaitForSeconds(1f);
        page_clone = Instantiate(page, book.transform);
        Destroy(page_clone, 2f);
        yield return new WaitForSeconds(0.1f);
        page_clone = Instantiate(page, book.transform);
        Destroy(page_clone, 2f);
        yield return new WaitForSeconds(0.1f);
        page_clone = Instantiate(page, book.transform);
        Destroy(page_clone, 2f);
        yield return new WaitForSeconds(0.1f);
        page_clone = Instantiate(page, book.transform);
        Destroy(page_clone, 2f);
        yield return new WaitForSeconds(0.1f);
        page_clone = Instantiate(page, book.transform);
        Destroy(page_clone, 2f);
        yield return new WaitForSeconds(0.1f);
        page_clone = Instantiate(page, book.transform);
        Destroy(page_clone, 2f);
        yield return new WaitForSeconds(0.1f);
        page_clone = Instantiate(page, book.transform);
        Destroy(page_clone, 2f);
        yield return new WaitForSeconds(0.1f);
        page_clone = Instantiate(page, book.transform);
        Destroy(page_clone, 2f);
        yield return new WaitForSeconds(0.1f);
        page_clone = Instantiate(page, book.transform);
        Destroy(page_clone, 2f);
        yield return new WaitForSeconds(0.1f);
        page_clone = Instantiate(page, book.transform);
        Destroy(page_clone, 2f);
        yield return new WaitForSeconds(0.1f);
        page_clone = Instantiate(page, book.transform);
        Destroy(page_clone, 2f);
        yield return new WaitForSeconds(0.1f);
        page_clone = Instantiate(page, book.transform);
        Destroy(page_clone, 2f);
        yield return new WaitForSeconds(0.1f);
        page_clone = Instantiate(page, book.transform);
        Destroy(page_clone, 2f);
        yield return new WaitForSeconds(0.2f);
        page_clone = Instantiate(page, book.transform);
        Destroy(page_clone, 2f);
        yield return new WaitForSeconds(0.3f);
        page_clone = Instantiate(page, book.transform);
        Destroy(page_clone, 2f);
        yield return new WaitForSeconds(0.4f);
        page_clone = Instantiate(page, book.transform);
        Destroy(page_clone, 2f);
        yield return new WaitForSeconds(7f);
        Destroy(page_clone);
        fade = true;
    }
}
