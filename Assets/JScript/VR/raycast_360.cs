﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR;

public class raycast_360 : MonoBehaviour
{
    public GameObject cam;
    Vector3 pos;
    public Image circle;
    public bool shot;

    public GameObject obj_1;
    public GameObject obj_2;
    public GameObject obj_3;
    public GameObject obj_4;
    public GameObject obj_5;
    public GameObject obj_6;
    public GameObject obj_7;

    public GameObject painting_1_obj;
    public GameObject painting_2_obj;
    public GameObject painting_3_obj;
    public GameObject painting_4_obj;
    public GameObject painting_5_obj;
    public GameObject painting_6_obj;
    public GameObject painting_7_obj;
    public GameObject 平面_obj;

    public Material mainmat;
    public Sprite orgpos;
    public Sprite painting_1;
    public Sprite painting_2;
    public Sprite painting_3;
    public Sprite painting_4;
    public Sprite painting_5;
    public Sprite painting_6;
    public Sprite painting_7;

    public Material paper_a;
    public Material paper_b;
    public Material paper_c;
    public Material paper_d;
    public Material red_paper;
    public Material wave_paper;
    public int check;
    public GameObject book_open;

    public Material 孔廟梁柱;
    public Material 樓梯;
    public Material 橘瓦;
    public Material 石地;
    public Material 紅瓦;
    public Material 門;
    public Material 黑瓦;
    public Material 風景土色A;
    public Material 風景紅牆;
    public Material 風景草;
    public Material 風景藍;
    public Material 風景黃;
    public Material 風景黑牆;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(loaddev_wait("Cardboard"));
        mainmat.mainTexture = orgpos.texture;
    }

    // Update is called once per frame
    void Update()
    {
        //pos = cam.transform.TransformPoint(Vector3.forward * 10);
        Debug.DrawLine(cam.transform.position, cam.transform.forward * 100, Color.red, 0.1f, true);

        if (shot)
        {
            Ray ray = new Ray(cam.transform.position, cam.transform.forward * 100);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.transform.tag == "painting")
                {
                    circle.fillAmount = circle.fillAmount + 0.5f * Time.deltaTime;
                    Debug.Log("aa");
                }
                if (hit.transform.tag == "wall")
                {
                    circle.fillAmount = 0;
                }

                if (circle.fillAmount == 1 && hit.transform.name == "paint_1")
                {
                    mainmat.mainTexture = painting_1.texture;
                    obj_1.SetActive(true);
                    StartCoroutine(pic_wait());

                    shot = false;
                    StartCoroutine(shot_wait(20));
                }
                if (circle.fillAmount == 1 && hit.transform.name == "paint_2")
                {
                    mainmat.mainTexture = painting_2.texture;
                    obj_2.SetActive(true);
                    if (check == 0)
                    {
                        paper_a.color = new Color(1, 1, 1, 1);
                        paper_b.color = new Color(1, 1, 1, 1);
                        paper_c.color = new Color(1, 1, 1, 1);
                        paper_d.color = new Color(1, 1, 1, 1);
                        red_paper.color = new Color(red_paper.color.r, red_paper.color.g, red_paper.color.b, 1);
                        wave_paper.color = new Color(1, 1, 1, 1);
                        check = 1;
                    }

                    shot = false;
                    StartCoroutine(shot_wait(25));
                }
                if (circle.fillAmount == 1 && hit.transform.name == "paint_3")
                {
                    mainmat.mainTexture = painting_3.texture;
                    孔廟梁柱.color = new Color(1, 1, 1, 0);
                    樓梯.color = new Color(1, 1, 1, 0);
                    橘瓦.color = new Color(1, 1, 1, 0);
                    石地.color = new Color(1, 1, 1, 0);
                    紅瓦.color = new Color(1, 1, 1, 0);
                    門.color = new Color(1, 1, 1, 0);
                    黑瓦.color = new Color(1, 1, 1, 0);
                    風景土色A.color = new Color(1, 1, 1, 1);
                    風景紅牆.color = new Color(1, 1, 1, 1);
                    風景草.color = new Color(1, 1, 1, 1);
                    風景藍.color = new Color(1, 1, 1, 1);
                    風景黃.color = new Color(1, 1, 1, 1);
                    風景黑牆.color = new Color(1, 1, 1, 1);

                    obj_3.SetActive(true);

                    shot = false;
                    StartCoroutine(shot_wait(30));
                }
                if (circle.fillAmount == 1 && hit.transform.name == "paint_4")
                {
                    mainmat.mainTexture = painting_4.texture;
                    obj_4.SetActive(true);
                    StartCoroutine(pic_wait());

                    shot = false;
                    StartCoroutine(shot_wait(37));
                }
                if (circle.fillAmount == 1 && hit.transform.name == "paint_5")
                {
                    mainmat.mainTexture = painting_5.texture;
                    obj_5.SetActive(true);

                    shot = false;
                    StartCoroutine(shot_wait(20));
                }
                if (circle.fillAmount == 1 && hit.transform.name == "paint_6")
                {
                    cam.transform.localEulerAngles = new Vector3(0,90,0);
                    mainmat.mainTexture = painting_6.texture;
                    obj_6.SetActive(true);
                    StartCoroutine(pic_wait());

                    shot = false;
                    StartCoroutine(shot_wait(23));
                }
                if (circle.fillAmount == 1 && hit.transform.name == "paint_7")
                {
                    cam.transform.localEulerAngles = new Vector3(0, 90, 0);
                    mainmat.mainTexture = painting_7.texture;
                    book_open.SetActive(true);
                    book_open.GetComponent<Page>().check = 0;
                    book_open.GetComponent<Page>().fade = false;
                    book_open.GetComponent<Page>().book_mat.color = new Color(1,1,1,1);
                    book_open.GetComponent<Page>().book_mat_L.color = new Color(1, 1, 1, 1);
                    book_open.GetComponent<Page>().book_mat_R.color = new Color(1, 1, 1, 1);
                    obj_7.SetActive(true);


                    shot = false;
                    StartCoroutine(shot_wait(13));
                }
                if (circle.fillAmount == 1 && hit.transform.name == "平面")
                {
                    hit.transform.GetComponent<BoxCollider>().enabled = false;
                    shot = false;
                    circle.fillAmount = 0;
                    check = 0;
                    Application.LoadLevel("Home");
                }
            }
        }
        else
        {
            //circle.fillAmount = 0;
            check = 0;
        }
    }
    IEnumerator shot_wait(int xx)
    {
        circle.fillAmount = 0;
        painting_1_obj.SetActive(false);
        painting_2_obj.SetActive(false);
        painting_3_obj.SetActive(false);
        painting_4_obj.SetActive(false);
        painting_5_obj.SetActive(false);
        painting_6_obj.SetActive(false);
        painting_7_obj.SetActive(false);
        平面_obj.SetActive(false);
        yield return new WaitForSeconds(2);
        shot = true;
        yield return new WaitForSeconds(xx);
        obj_1.SetActive(false);
        obj_2.SetActive(false);
        obj_3.SetActive(false);
        obj_4.SetActive(false);
        obj_5.SetActive(false);
        obj_6.SetActive(false);
        obj_7.SetActive(false);
        obj_1.transform.localPosition = new Vector3(20000f, 0, 0.5f);
        obj_4.transform.localPosition = new Vector3(20000f, 0.7f, 0.2f);
        obj_6.transform.localPosition = new Vector3(20000f, -0.4f, -0.6f);
        mainmat.mainTexture = orgpos.texture;
        painting_1_obj.SetActive(true);
        painting_2_obj.SetActive(true);
        painting_3_obj.SetActive(true);
        painting_4_obj.SetActive(true);
        painting_5_obj.SetActive(true);
        painting_6_obj.SetActive(true);
        painting_7_obj.SetActive(true);
        平面_obj.SetActive(true);
    }
    IEnumerator pic_wait()
    {
        yield return new WaitForSeconds(2f);
        obj_6.transform.localPosition = new Vector3(252.6f, -0.4f, -0.6f);
        obj_1.transform.localPosition = new Vector3(252.6f, 0, 0.5f);
        obj_4.transform.localPosition = new Vector3(252.6f, 0.7f, 0.2f);
    }
    IEnumerator loaddev_wait(string newdev)
    {
        XRSettings.LoadDeviceByName(newdev);
        yield return null;
        XRSettings.enabled = true;
    }
}
