﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.Playables;
using UnityEngine.XR;
using UnityEngine.UI;

public class Main_VR_sun : MonoBehaviour
{
    public bool sun_firestop;

    public int sun_number;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //太陽
        if (sun_number == 0)
        {
            StartCoroutine(ani_2_fn());
            sun_number = 1;
        }
    }
    IEnumerator ani_2_fn()
    {
        yield return new WaitForSeconds(15f);
        sun_firestop = true;
    }
}
