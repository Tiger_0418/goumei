﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class firetrigger_1_VR : MonoBehaviour
{
    public GameObject st_1;
    public GameObject candle;
    public GameObject mn;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (st_1.GetComponent<PlayableDirector>().enabled == true)
        {
            if (candle.GetComponent<MeshRenderer>().material.color.a > 0)
            {
                candle.GetComponent<MeshRenderer>().material.color = new Color(candle.GetComponent<MeshRenderer>().material.color.r, candle.GetComponent<MeshRenderer>().material.color.g, candle.GetComponent<MeshRenderer>().material.color.b, candle.GetComponent<MeshRenderer>().material.color.a - 0.01f);
            }
            if (candle.GetComponent<MeshRenderer>().material.color.a <= 0)
            {
                candle.GetComponent<MeshRenderer>().material.color = new Color(candle.GetComponent<MeshRenderer>().material.color.r, candle.GetComponent<MeshRenderer>().material.color.g, candle.GetComponent<MeshRenderer>().material.color.b, 0);
                candle.SetActive(false);
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "candle")
        {
            st_1.GetComponent<PlayableDirector>().enabled = true;
            mn.GetComponent<Main_VR_jagu>().jagu_touch = true;
        }
    }
}
